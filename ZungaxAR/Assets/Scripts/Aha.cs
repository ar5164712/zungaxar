using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aha : MonoBehaviour
{
    [SerializeField]
    private GameObject chimbada;

    [SerializeField]
    private GameObject panel;

    [SerializeField]
    private GameObject panelAja;

    public void Start()
    {
        Time.timeScale = 1.0f;
    }
    public void AparecerMonda()
    {
        Time.timeScale = 1f;
        chimbada.SetActive(true);
        panel.SetActive(false);
        panelAja.SetActive(true);
    }
    public void PararMonda()
    {
        Time.timeScale = 0;
    }
}
