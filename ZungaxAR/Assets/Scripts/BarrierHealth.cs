using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class BarrierHealth : MonoBehaviour
{
    public Image[] hearts;
    public int totalHearts;

    private void Start()
    {
        totalHearts = hearts.Length;
    }
    public void TakeDamage()
    {
        if (totalHearts > 0)
        {
            totalHearts--;
            hearts[totalHearts].gameObject.SetActive(false);
        }

        // Verificar si se acabaron los corazones
        if (totalHearts <= 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
